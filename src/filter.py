from graphics import *
from datetime import datetime


def main():
    """runs the program"""
    run_editor()


def run_editor():
    """creates the image window and the control panel, calls
    the relevant functions, and runs the program"""

    img = Image(Point(0, 0), "image.gif")
    win = create_image_win(img)
    img.draw(win)
    control_win = create_control_win()

    inc_brightness_butt = create_inc_brightness_butt(control_win)
    decr_brightness_butt = create_decr_brightness_butt(control_win)
    save_butt = create_save_butt(control_win)
    flip_butt = create_flip_butt(control_win)
    mirror_butt = create_mirror_butt(control_win)
    blur_butt = create_blur_butt(control_win)
    distort_butt = create_distort_butt(control_win)
    add_frame_butt = create_add_frame_butt(control_win)
    keep_red_butt = create_keep_red_butt(control_win)
    inc_contrast_butt = create_inc_contrast_butt(control_win)
    decr_contrast_butt = create_decr_contrast_butt(control_win)
    grayscale_butt = create_grayscale_butt(control_win)
    pixify_butt = create_pixify_butt(control_win)
    popart_butt = create_popart_butt(control_win)
    sepia_butt = create_sepia_butt(control_win)
    original_butt = create_original_butt(control_win)
    close_butt = create_close_butt(control_win)

    matrix = create_matrix(img)

    while True:
        mouse_click = control_win.checkMouse()

        if mouse_click is not None:
            start = datetime.now()

            if is_inside(inc_brightness_butt, mouse_click):
                edit_inc_brightness(img, matrix)

            if is_inside(decr_brightness_butt, mouse_click):
                edit_decr_brightness(img, matrix)

            if is_inside(flip_butt, mouse_click):
                edit_flip(img, matrix)

            if is_inside(mirror_butt, mouse_click):
                edit_mirror(img, matrix)

            if is_inside(blur_butt, mouse_click):
                edit_blur(img, matrix)

            if is_inside(save_butt, mouse_click):
                img.save("my_picture.gif")

            if is_inside(distort_butt, mouse_click):
                edit_distort_colors(img, matrix)

            if is_inside(add_frame_butt, mouse_click):
                edit_add_frame(img, matrix)

            if is_inside(grayscale_butt, mouse_click):
                edit_grayscale(img, matrix)

            if is_inside(pixify_butt, mouse_click):
                edit_pixify(img, matrix)

            if is_inside(popart_butt, mouse_click):
                edit_popart(img, matrix)

            if is_inside(keep_red_butt, mouse_click):
                edit_keep_only_red(img, matrix)

            if is_inside(inc_contrast_butt, mouse_click):
                edit_inc_contrast(img, matrix)

            if is_inside(decr_contrast_butt, mouse_click):
                edit_decr_contrast(img, matrix)

            if is_inside(original_butt, mouse_click):
                img = edit_original(img, win)

            if is_inside(sepia_butt, mouse_click):
                edit_sepia(img, matrix)

            if is_inside(close_butt, mouse_click):
                break

            stop = datetime.now()
            delta = (stop - start)
            seconds = delta.total_seconds()
            print("Operation took", int(seconds), "seconds.")

    win.close()
    control_win.close()


def edit_inc_brightness(img, matrix):
    """increases the brightness"""
    width, height = set_values(img)

    for x in range(width):
        for y in range(height):
            r, g, b = img.getPixel(x, y)
            l = int(calculate_luminance(r, g, b))
            rn = r + int(l / 2)
            gn = g + int(l / 2)
            bn = b + int(l / 2)
            matrix[x][y] = fix_rgb(rn, gn, bn)
    matrix_to_img(matrix, img)


def edit_decr_brightness(img, matrix):
    """decreases brightness"""
    width, height = set_values(img)

    for x in range(width):
        for y in range(height):
            r, g, b = img.getPixel(x, y)
            l = int(calculate_luminance(r, g, b))
            rn = r - int(l / 2)
            gn = g - int(l / 2)
            bn = b - int(l / 2)
            matrix[x][y] = fix_rgb(rn, gn, bn)
    matrix_to_img(matrix, img)


def edit_flip(img, matrix):
    """flips the image upside down"""
    width, height = set_values(img)

    for x in range(width):
        for y in range(height):
            r, g, b = img.getPixel(x, y)
            matrix[x][height - y - 1] = r, g, b
    matrix_to_img(matrix, img)


def edit_mirror(img, matrix):
    """mirrors the image on y axis"""
    width, height = set_values(img)

    for x in range(width):
        for y in range(height):
            r, g, b = img.getPixel(x, y)
            matrix[-x - 1][y] = r, g, b
    matrix_to_img(matrix, img)


def edit_blur(img, matrix):
    """takes the average of the neighbouring pixels for each pixel,
    in the range of (size), and assign the average value to that pixel."""
    width, height = set_values(img)
    size = 4

    for x in range(size, width - int(size / 2)):
        for y in range(size, height - int(size / 2)):
            r_sum = 0
            g_sum = 0
            b_sum = 0
            for i in range(size):
                for j in range(size):
                    r, g, b = img.getPixel(x - i, y - j)
                    r_sum += r
                    g_sum += g
                    b_sum += b
            r_avg = int(r_sum / (size ** 2))
            g_avg = int(g_sum / (size ** 2))
            b_avg = int(b_sum / (size ** 2))
            matrix[x][y] = r_avg, g_avg, b_avg
    matrix_to_img(matrix, img)


def edit_distort_colors(img, matrix):
    """does strange stuff with colors <3"""
    width, height = set_values(img)

    for x in range(width):
        for y in range(height):
            r, g, b = img.getPixel(x, y)
            r_new = int((r * 0.8) + (g * 0.1) + (b * 0.1))
            b_new = int((r * 0.1) + (g * 0.8) + (b * 0.1))
            g_new = int((r * 0.1) + (g * 0.1) + (b * 0.8))
            r_new, g_new, b_new = fix_rgb(r_new, g_new, b_new)
            matrix[x][y] = r_new, g_new, b_new
    matrix_to_img(matrix, img)


def edit_add_frame(img, matrix):
    """adds a gray frame around the image."""
    width, height = set_values(img)
    rn = 206
    gn = 208
    bn = 215

    # aşağı ve yukarı çerçeveler için
    for x in range(width):
        for y in range(13):
            matrix[x][y] = rn, gn, bn
        for z in range(height - 10, height):
            matrix[x][z] = rn, gn, bn

    # yan çerçeveler için
    for a in range(10, height - 10):
        for b in range(13):
            matrix[b][a] = rn, gn, bn
        for c in range(width - 10, width):
            matrix[c][a] = rn, gn, bn

    matrix_to_img(matrix, img)


def edit_keep_only_red(img, matrix):
    """intensifies the red colors in the image, while making
    everything else gray-ish"""
    width, height = set_values(img)

    for x in range(width):
        for y in range(height):
            r, g, b = img.getPixel(x, y)
            l = int(calculate_luminance(r, g, b))
            if r < 220:
                r = l
            else:
                r = int(r * 1.5)
            r, g, b = fix_rgb(r, g, b)
            matrix[x][y] = r, l, l
    matrix_to_img(matrix, img)


def edit_inc_contrast(img, matrix):
    """increases color contrast of the image by increasing the
    difference between the strongest colors."""
    width, height = set_values(img)

    for x in range(width):
        for y in range(height):
            r, g, b = img.getPixel(x, y)
            max_rgb = max(r, g, b)
            min_rgb = min(r, g, b)
            l = calculate_luminance(r, g, b)
            if l > 127:
                if max_rgb == r:
                    r = r + 20
                if max_rgb == g:
                    g = g + 20
                if max_rgb == b:
                    b = b + 20
            if l <= 127:
                if min_rgb == r:
                    r = r - 20
                if min_rgb == g:
                    g = g - 20
                if min_rgb == b:
                    b = b - 20
            rn, gn, bn = fix_rgb(r, g, b)
            matrix[x][y] = rn, gn, bn
    matrix_to_img(matrix, img)


def edit_decr_contrast(img, matrix):
    """decreases color contrast of the image, by decreasing the
    difference between the strongest colors."""
    width, height = set_values(img)

    for x in range(width):
        for y in range(height):
            r, g, b = img.getPixel(x, y)
            max_rgb = max(r, g, b)
            min_rgb = min(r, g, b)
            l = calculate_luminance(r, g, b)
            if l > 127:
                if max_rgb == r:
                    r = r - 20
                if max_rgb == g:
                    g = g - 20
                if max_rgb == b:
                    b = b - 20
            if l <= 127:
                if min_rgb == r:
                    r = r + 20
                if min_rgb == g:
                    g = g + 20
                if min_rgb == b:
                    b = b + 20
            rn, gn, bn = fix_rgb(r, g, b)
            matrix[x][y] = rn, gn, bn
    matrix_to_img(matrix, img)


def edit_grayscale(img, matrix):
    """turns image into grayscale"""
    width, height = set_values(img)

    for x in range(width):
        for y in range(height):
            r, g, b = img.getPixel(x, y)
            l = int(calculate_luminance(r, g, b))
            matrix[x][y] = l, l, l
    matrix_to_img(matrix, img)


def edit_pixify(img, matrix):  # the whole thing is a bit yellow-ish.. Idk why
    """takes the rgb value of the pixel in the center, and assigns it
    to other neighboring pixels in the range of pixel_size"""
    width, height = set_values(img)
    pixel_size = 6

    for x in range(pixel_size, width, pixel_size):
        for y in range(pixel_size, height, pixel_size):
            r, g, b = img.getPixel(x - int(pixel_size / 2), y - int(pixel_size / 2))

            for i in range(pixel_size):
                for j in range(pixel_size):
                    matrix[x - i][y - j] = r, g, b

    matrix_to_img(matrix, img)


def edit_popart(img, matrix):
    """creates a popart image based on the given image, with the original
    on the top left, and so on."""
    width, height = set_values(img)

    for x in range(0, width, 2):
        for y in range(0, height, 2):
            r, g, b = img.getPixel(x, y)
            matrix[int(x / 2)][int(y / 2)] = r, g, b
            matrix[int(width / 2) + int(x / 2)][int(y / 2)] = r, 0, 0
            matrix[int(x / 2)][int(height / 2) + int(y / 2)] = 0, b, 0
            matrix[int(width / 2) + int(x / 2)][int(height / 2) + int(y / 2)] = 0, 0, g

    matrix_to_img(matrix, img)


def edit_original(img, win):
    """Returns to the original image"""
    width, height = set_values(img)
    img.undraw()
    original_image = Image(Point(0, 0), "image.gif")
    original_image.anchor = Point(width / 2, height / 2)
    original_image.draw(win)

    return original_image


def edit_sepia(img, matrix):
    """the sepia filter"""
    width, height = set_values(img)
    sepia = 20

    for x in range(width):
        for y in range(height):
            r, g, b = img.getPixel(x, y)
            rn = gn = bn = int(calculate_luminance(r, g, b))
            rn = rn + (sepia * 2)
            bn = bn - sepia
            rn, gn, bn = fix_rgb(rn, gn, bn)
            matrix[x][y] = rn, gn, bn
    matrix_to_img(matrix, img)


def calculate_luminance(r, g, b):
    """Calculates effective luminance"""
    luminance = (r * 0.3) + (g * 0.59) + (b * 0.11)
    return luminance


def fix_rgb(r, g, b):
    """makes sure that r, g, b is in [0, 255] range."""
    if r < 0:
        r = 0
    if r > 255:
        r = 255
    if g < 0:
        g = 0
    if g > 255:
        g = 255
    if b < 0:
        b = 0
    if b > 255:
        b = 255

    return r, g, b


def set_values(img):
    """Sets the values for width and height"""
    width = img.getWidth()
    height = img.getHeight()
    return width, height


def create_matrix(img):
    """creates the matrix and stores r, g, b values of the image"""
    width, height = set_values(img)
    matrix = [[0, 0, 0] * height for _ in range(width)]

    for x in range(width):
        for y in range(height):
            matrix[x][y] = img.getPixel(x, y)
    return matrix


def matrix_to_img(matrix, img):
    """Updates img to reflect the r, g, b values of the given matrix"""
    width, height = set_values(img)

    for x in range(width):
        for y in range(height):
            r, g, b = matrix[x][y]
            img.setPixel(x, y, color_rgb(r, g, b))


def create_image_win(img):
    """creates the image window"""
    width, height = set_values(img)
    img.anchor = Point(width / 2, height / 2)
    win = GraphWin("Image", width, height)
    return win


def create_control_win():
    """Creates the control panel window"""
    control_win = GraphWin("Control_Panel", 450, 300)
    control_win.setCoords(0, 0, 450, 300)
    control_win.setBackground(color_rgb(255, 250, 240))
    return control_win


def create_inc_brightness_butt(control_win):
    inc_brightness_butt = Rectangle(Point(20, 270), Point(50, 290))
    inc_brightness_butt.draw(control_win)
    inc_brightness_butt.setFill(color_rgb(179, 109, 109))
    text = Text(Point(125, 280), "Increase brightness")
    text.draw(control_win)
    return inc_brightness_butt


def create_decr_brightness_butt(control_win):
    decr_brightness_butt = Rectangle(Point(20, 240), Point(50, 260))
    decr_brightness_butt.draw(control_win)
    decr_brightness_butt.setFill(color_rgb(179, 109, 109))
    text = Text(Point(129, 250), "Decrease brightness")
    text.draw(control_win)
    return decr_brightness_butt


def create_sepia_butt(control_win):
    sepia_butt = Rectangle(Point(20, 210), Point(50, 230))
    sepia_butt.draw(control_win)
    sepia_butt.setFill(color_rgb(179, 109, 109))
    text = Text(Point(80, 220), "Sepia")
    text.draw(control_win)
    return sepia_butt


def create_flip_butt(control_win):
    flip_butt = Rectangle(Point(20, 180), Point(50, 200))
    flip_butt.draw(control_win)
    flip_butt.setFill(color_rgb(179, 109, 109))
    text = Text(Point(70, 190), "Flip")
    text.draw(control_win)
    return flip_butt


def create_mirror_butt(control_win):
    mirror_butt = Rectangle(Point(20, 150), Point(50, 170))
    mirror_butt.draw(control_win)
    mirror_butt.setFill(color_rgb(179, 109, 109))
    text = Text(Point(75, 160), "Mirror")
    text.draw(control_win)
    return mirror_butt


def create_blur_butt(control_win):
    blur_butt = Rectangle(Point(20, 120), Point(50, 140))
    blur_butt.draw(control_win)
    blur_butt.setFill(color_rgb(179, 109, 109))
    text = Text(Point(70, 130), "Blur")
    text.draw(control_win)
    return blur_butt


def create_distort_butt(control_win):
    distort_butt = Rectangle(Point(20, 90), Point(50, 110))
    distort_butt.draw(control_win)
    distort_butt.setFill(color_rgb(179, 109, 109))
    text = Text(Point(103, 100), "Distort colors")
    text.draw(control_win)
    return distort_butt


def create_add_frame_butt(control_win):
    add_frame_butt = Rectangle(Point(20, 60), Point(50, 80))
    add_frame_butt.draw(control_win)
    add_frame_butt.setFill(color_rgb(179, 109, 109))
    text = Text(Point(111, 70), "Add gray frame")
    text.draw(control_win)
    return add_frame_butt


def create_keep_red_butt(control_win):
    keep_red_butt = Rectangle(Point(20, 30), Point(50, 50))
    keep_red_butt.draw(control_win)
    keep_red_butt.setFill(color_rgb(179, 109, 109))
    text = Text(Point(106, 40), "Keep only red")
    text.draw(control_win)
    return keep_red_butt


def create_inc_contrast_butt(control_win):
    inc_contrast_butt = Rectangle(Point(225, 270), Point(255, 290))
    inc_contrast_butt.draw(control_win)
    inc_contrast_butt.setFill(color_rgb(179, 109, 109))
    text = Text(Point(317, 280), " Increase contrast")
    text.draw(control_win)
    return inc_contrast_butt


def create_decr_contrast_butt(control_win):
    decr_contrast_butt = Rectangle(Point(225, 240), Point(255, 260))
    decr_contrast_butt.draw(control_win)
    decr_contrast_butt.setFill(color_rgb(179, 109, 109))
    text = Text(Point(325, 250), "Decrease contrast")
    text.draw(control_win)
    return decr_contrast_butt


def create_grayscale_butt(control_win):
    grayscale_butt = Rectangle(Point(225, 210), Point(255, 230))
    grayscale_butt.draw(control_win)
    grayscale_butt.setFill(color_rgb(179, 109, 109))
    text = Text(Point(295, 220), "Grayscale")
    text.draw(control_win)
    return grayscale_butt


def create_pixify_butt(control_win):
    pixify_butt = Rectangle(Point(225, 180), Point(255, 200))
    pixify_butt.draw(control_win)
    pixify_butt.setFill(color_rgb(179, 109, 109))
    text = Text(Point(279, 190), "Pixify")
    text.draw(control_win)
    return pixify_butt


def create_popart_butt(control_win):
    popart_butt = Rectangle(Point(225, 150), Point(255, 170))
    popart_butt.draw(control_win)
    popart_butt.setFill(color_rgb(179, 109, 109))
    text = Text(Point(283, 160), "Pop art")
    text.draw(control_win)
    return popart_butt


def create_save_butt(control_win):
    save_butt = Rectangle(Point(225, 90), Point(255, 110))
    save_butt.draw(control_win)
    save_butt.setFill(color_rgb(179, 109, 109))
    text = Text(Point(297, 100), "Save to file")
    text.draw(control_win)
    return save_butt


def create_original_butt(control_win):
    original_butt = Rectangle(Point(225, 60), Point(255, 80))
    original_butt.draw(control_win)
    original_butt.setFill(color_rgb(179, 109, 109))
    text = Text(Point(285, 70), "Original")
    text.draw(control_win)
    return original_butt


def create_close_butt(control_win):
    close_butt = Rectangle(Point(225, 30), Point(255, 50))
    close_butt.draw(control_win)
    close_butt.setFill(color_rgb(255, 60, 60))
    text = Text(Point(280, 40), "Close")
    text.draw(control_win)
    return close_butt


def is_inside(rectangle, mouse_point):
    """checks if the mouse click is in a particular rectangle, if yes,
    returns True"""
    rect_point_one = rectangle.getP1()
    rect_point_two = rectangle.getP2()
    x_one = rect_point_one.getX()
    y_one = rect_point_one.getY()
    x_two = rect_point_two.getX()
    y_two = rect_point_two.getY()
    point_x = mouse_point.getX()
    point_y = mouse_point.getY()
    if x_one <= point_x <= x_two and y_one <= point_y <= y_two:
        return True
    else:
        return False


if __name__ == '__main__':
    main()
