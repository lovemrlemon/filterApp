Thank you so much for taking time to read this!

Please do share your thoughts on what I can improve in this project. :)

## Overview

This project is one of the introductory python assignments from Bergen University's
INF109 course.

Python 3.8 is used for development, and should be installed in order to run the app.

graphics.py is the library used for the GUI provided by the INF109 course. The script
for the game is in the filter.py file. 

## Usage

In order to start the app, the user must run filter.py file using the command below:
```python filter.py```

Two windows will open once the app runs: the control GUI and the image. 

The user can chose which filter they want to have on the image. The filters will have 
cumulative effect on the image. Meaning, if the user chooses "Sepia", then "Decrease brightness", 
the latter filter will be performed on the Sepia image.

The ```Save to file``` button saves the current image in the same directory as the source image.

The ```Original``` button reverts the image to its original colors.

In each process, the app will inform the user how long the process took in seconds in the terminal.

### Restrictions

The name of the image file is hard coded. It can be changed in the code, but the output file
of the "Save to File" feature will still be in the same folder where the source code is. 


## Further Improvements

As it currently is, the app is very slow (a filter takes around 12 seconds to take effect).
It could be much more efficient. 

Of course more fun filters can be added :)